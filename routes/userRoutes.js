const express = require("express");
const router = express.Router();
const userController = require("../controller/user.controller");
const validateRequest = require("../middleware/validateRequest");
const { loginUserSchema, createUserSchema } = require("../schema/user.schema");
const authPolicy = require("../utils/auth.policy");

router.post(
  "/signup",
  validateRequest(createUserSchema),
  userController.signUpUser,
);
router.post(
  "/login",
  validateRequest(loginUserSchema),
  userController.loginUser,
);

module.exports = router;
