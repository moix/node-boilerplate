"use strict";
module.exports = (sequelize, DataTypes) => {
  var Users = sequelize.define(
    "User",
    {
      fullName: {
        type: DataTypes.STRING(255),
        allowNull: true,
      },
      email: {
        type: DataTypes.STRING(100),
        allowNull: false,
        unique: {
          msg: "This Email already exists",
        },
      },
      password: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      phoneNo: {
        type: DataTypes.STRING(22),
        allowNull: false,
      },
      role: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      status: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
    },

    {
      freezeTableName: true,
    },
  );

  Users.associate = function (models) {
    // Users.hasOne(models.Admin, {
    //   as: "admin",
    //   constraints: false,
    //   foreignKey: {
    //     name: "user_id",
    //     allowNull: false,
    //     field: "user_id",
    //   },
    // });
    // Users.hasMany(models.TourBookings, {
    //   as: "tourAgencyBookings",
    //   constraints: false,
    //   foreignKey: {
    //     name: "agency_id",
    //     allowNull: false,
    //     field: "agency_id",
    //   },
    // });
    // Users.hasOne(models.Hotel, {
    //   as: "hotel",
    //   constraints: false,
    //   foreignKey: {
    //     name: "user_id",
    //     allowNull: false,
    //     field: "user_id",
    //   },
    // });
    // Users.hasOne(models.TourAgency, {
    //   as: "tourAgency",
    //   constraints: false,
    //   foreignKey: {
    //     name: "user_id",
    //     allowNull: false,
    //     field: "user_id",
    //   },
    // });
    // Users.hasOne(models.Transport, {
    //   as: "transport",
    //   constraints: false,
    //   foreignKey: {
    //     name: "user_id",
    //     allowNull: false,
    //     field: "user_id",
    //   },
    // });
  };

  return Users;
};
