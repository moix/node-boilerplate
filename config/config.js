const dotenv = require("dotenv");
const path = require("path");
console.log(`Your port is ${process.env.DB_USER}`);
module.exports = {
  development: {
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: "ezsaffer",
    host: "localhost",
    dialect: "mysql",
    logging: false,
    timezone: "+05:00",
    force: true,
  },
  production: {
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: "planet",
    host: process.env.DB_URL,
    dialect: "mysql",
    logging: false,
    timezone: "+05:00",
    dialectOptions: {
      ssl: {
        rejectUnauthorized: true,
      },
    },
  },
};
